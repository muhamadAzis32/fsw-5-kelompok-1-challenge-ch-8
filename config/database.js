require("dotenv").config();

const {
  DB_USER = "azis",
  DB_PASSWORD = "",
  DB_NAME = "db_challenge8",
  DB_HOST = "127.0.0.1",
  DB_PORT = "5432",
} = process.env;


module.exports = {
  development: {
    username: DB_USER,
    password: DB_PASSWORD,
    database: `${DB_NAME}`,
    host: DB_HOST,
    // port: DB_PORT,
    dialect: "postgres",
    ssl: true,
  },
  test: {
    username: DB_USER,
    password: DB_PASSWORD,
    database: `${DB_NAME}_test`,
    host: DB_HOST,
    port: DB_PORT,
    dialect: "postgres"
  },
  production: {
    username: "dccfpvkgctpzaf",
    password: "b9c62ce4cd6bf4fbac51ebd1572f674d5b5b20267e8e9447b1e3c03a7b06d911",
    database: "d6tps3l2mtpukn",
    host: "ec2-34-198-186-145.compute-1.amazonaws.com",
    dialect: "postgres",
    dialectOptions: {
      "ssl": {
        "require": true,
        "rejectUnauthorized": false
      }
    }
  }
}
