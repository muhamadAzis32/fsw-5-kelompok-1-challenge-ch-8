module.exports = {
    "env": {
        "browser": true,
        "es2021": true
    },
    "extends": "eslint:recommended",
    "parserOptions": {
        "ecmaVersion": "latest",
        // "sourceType": "module"
    },
    "rules": {
        // "semi": ["error", "always"],
        // "quotes": ["error", "double"], 
        // "curly": "error",
        "no-undef": "off",
        "no-extra-boolean-cast": "off",
        "no-unused-vars": "off",
    }
};
